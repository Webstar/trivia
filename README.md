# Trivia

Requirements located [here](https://gist.github.com/severnsc/e09f4f8742b7dd91af9c422d6f210a57)

React front end application with [redux](https://redux.js.org) and [material ui](https://material-ui.com)

To run locally:

```bash
npm install
npm run start
```

To do a production build:

```bash
npm run build
```