import {
  createStore,
  applyMiddleware
} from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import promise from "redux-promise-middleware";
import triviaReducer from "data/rootReducer";

const middleware = [
  thunk,
  promise({
    promiseTypeSuffixes: ["START", "SUCCESS", "ERROR"],
  }),
  logger()
];

const createWithMiddleware = applyMiddleware(...middleware)(createStore);

function getRootReducer() {
  return triviaReducer;
}

export default function configureStore(initialState) {
  const store = createWithMiddleware(triviaReducer, initialState);

  if (module.hot) {
    module.hot.accept("./rootReducer", () => {
      store.replaceReducer(getRootReducer());
    });
  }

  return store;
}
