import {getJSON} from "data/fetch";
import getAPIURL from "data/util";

export const LOAD_QUESTIONS = "LOAD_QUESTIONS";
export const SET_OPTIONS = "SET_OPTIONS";
export const SET_ANSWER = "SET_ANSWER";
export const GENERATE_SCORE = "GENERATE_SCORE";

export function loadQuestions() {
  return (dispatch, getState) => {
    const st = getState();
    return dispatch({
      type: LOAD_QUESTIONS,
      payload: {
        promise: getJSON(getAPIURL(st.options.difficulty, st.options.numQuestions))
      }
    });
  };
}

export function setOptions(opts) {
  return dispatch => {
    return dispatch({
      type: SET_OPTIONS,
      payload: opts
    });
  };
}

export function setAnswer(i, answer, time){
  return dispatch => {
    return dispatch({
      type: SET_ANSWER,
      payload: {
        i,
        answer,
        time,
      }
    });
  };
}

export function generateScore(){
  return dispatch => {
    return dispatch({
      type: GENERATE_SCORE,
    });
  };
}
