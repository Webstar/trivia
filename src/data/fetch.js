let AUTH_TOKEN = null;

export function setAuthToken(token) {
  AUTH_TOKEN = token;
}

export function getJSON(url, params = {}) {
  params.headers = prepareHeaders(params.headers);
  return fetch(url, params)
    .then(checkResponseCode)
    .then((resp) => {return resp.json();});
}

export function prepareHeaders(headers = {}) {
  // if (!headers.accept) {
  //   headers.accept = "application/json";
  // }
  // if (!headers["content-type"]) {
  //   headers["content-type"] = "application/json";
  // }
  // if (AUTH_TOKEN) {
  //   headers.Authorization = `Token ${AUTH_TOKEN}`;
  // }
  return headers;
}

export function checkResponseCode(resp) {
  if (resp.status >= 400) {
    const err = new Error(`Error ${resp.status} from ${resp.url}`);
    err.response = resp;
    throw err;
  }
  return resp;
}
