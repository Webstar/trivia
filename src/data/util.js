export default function getAPIURL(difficulty, numQuestions) {
  return `https://opentdb.com/api.php?amount=${numQuestions}&difficulty=${difficulty}&type=boolean`;
}
