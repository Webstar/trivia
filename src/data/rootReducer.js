import * as actions from "data/actions";

function getInitialState() {
  return {
    questions: [],
    options: {
      numQuestions: 10,
      difficulty: 'hard',
    },
    score: -1,
  };
}

export default function triviaReducer(state = getInitialState(), action) {
  switch (action.type) {

    case `${actions.LOAD_QUESTIONS}_SUCCESS`: {
      return Object.assign({}, state, {
        questions: action.payload.results,
        score: -1,
      });
    }

    case actions.SET_OPTIONS: {
      return Object.assign({}, state, {
        options: action.payload,
      });
    }

    case actions.SET_ANSWER: {
      let questions = state.questions;
      questions[action.payload.i].answer = action.payload.answer.toString();
      questions[action.payload.i].time = action.payload.time;
      return Object.assign({}, state, {
        questions,
      });
    }

    case actions.GENERATE_SCORE: {
      let score = 0;
      let count = 0;
      state.questions.map(q => {
        const correct = q.answer.toLowerCase() === q.correct_answer.toLowerCase();
        if (correct){
          count += 1;
          score += 5;
        }
        if (q.time < 10 && correct){
          score += 2;
        }
        else if (q.time < 30 && correct){
          score += 1;
        }
        else if (q.time > 120 && correct){
          score -= 1;
        }
        else if (q.time > 300 && correct){
          score -= 2;
        }
      });
      return Object.assign({}, state, {
        score,
        count,
      });
    }

    default:
      return state;
  }
}
