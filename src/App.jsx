import React from "react";
import PropTypes from "prop-types";
import {
  HashRouter,
  withRouter,
} from "react-router-dom";
import {hot} from "react-hot-loader";

import Frame from "scenes/Frame";

import "./styles.scss";


class ScrollToTop extends React.Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }
  render() {
    return <div>{this.props.children}</div>;
  }
}

const ScrollToTopWithRouter = withRouter(ScrollToTop);

ScrollToTop.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
  location: PropTypes.object
};

class App extends React.Component {
  render() {
    return (
      <HashRouter>
        <ScrollToTopWithRouter>
          <Frame/>
        </ScrollToTopWithRouter>
      </HashRouter>
    );
  }
}

const HotApp = hot(module)(App);
export default HotApp;
