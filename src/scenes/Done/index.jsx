import React from "react";
import {withRouter, Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import CheckCircle from '@material-ui/icons/CheckCircle';
import Cancel from '@material-ui/icons/Cancel';

import {generateScore} from "data/actions";

import "./styles.scss";

class Done extends React.Component {
  componentDidMount = () => {
    this.props.dispatch(generateScore());
  }

  render() {
    if (this.props.questions.length > 0 && this.props.score > -1){
      return (
        <Card className="summary-card">
          <CardHeader title="Trivia Summary"/>
          <CardActionArea className='content'>
            <CardContent>
              <Typography component="p" className="score">
                You scored: {this.props.score}!!
              </Typography>
              <Typography component="p" className="score">
                {this.props.count}/{this.props.questions.length} correct for {this.props.count/this.props.questions.length*100}%
              </Typography>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Question</TableCell>
                    <TableCell>Answer</TableCell>
                    <TableCell>Correct Answer</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.props.questions.map((row,i) => {
                    return (
                      <TableRow key={i}>
                        <TableCell dangerouslySetInnerHTML={{__html: row.question}}></TableCell>
                        <TableCell>{row.answer}</TableCell>
                        <TableCell>{row.correct_answer.toLowerCase()}</TableCell>
                        <TableCell>
                          {row.correct_answer.toLowerCase() === row.answer.toLowerCase() ? 
                              <CheckCircle style={{color: '#00AA00'}}/>
                            :
                              <Cancel style={{color: '#AA0000'}}/>
                          }
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>

              <Typography component="p" className="scoring-desc">
                Points are calculated by awarding 5 for a correct answer then adding a bonus for answering quicker.  Answering correctly but taking too long, subtracts points.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Link
              to="/"
            >
              <Button className="full-btn" size="small" color="primary" onClick={this.setTrue}>
                Return to Start Screen
              </Button>
            </Link>
          </CardActions>
        </Card>
      );
    }
    else if (this.props.questions.length > 0 && this.props.score === -1){
      return (
        <Card className="summary-card">
          <CardHeader title="Error loading quiz answers..."/>
          <CardActions>
            <Link
              to="/"
            >
              <Button className="full-btn" size="small" color="primary" onClick={this.setTrue}>
                Return to Start Screen
              </Button>
            </Link>
          </CardActions>
        </Card>
      );
    }
    else {
      return (
        <Card className="summary-card">
          <CardHeader title="Loading...."/>
        </Card>
      );
    }
  }
}

Done.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object,
  questions: PropTypes.array,
  score: PropTypes.number,
  count: PropTypes.number,
};

export default withRouter(connect(
  (state) => {return state;},
  null
)(Done));