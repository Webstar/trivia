import React from "react";
import PropTypes from "prop-types";
import {withRouter} from "react-router-dom";

import "./styles.scss";

class NotFound extends React.PureComponent {
  render() {
    return (
      <div className="not-found">
        <h1>404 - Not Found</h1>
        <a
          className="basic-button"
          onClick={() => {this.props.history.goBack();}}
        >
          Take me back
        </a>
      </div>
    );
  }
}

NotFound.propTypes = {
  history: PropTypes.object
};

export default withRouter(NotFound);
