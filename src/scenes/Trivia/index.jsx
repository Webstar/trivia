import React from "react";
import {withRouter, Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import triviaImage from '../../assets/images/trivia.png';


import "./styles.scss";

class Trivia extends React.Component {
  render() {
    return (
      <Card>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Trivia Sign"
            width="100%"
            className={{objectFit: 'cover'}}
            style={{maxWidth: '100%'}}
            image={triviaImage}
            title="Trivia Image"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              Welcome to G2i Trivia
            </Typography>
            <Typography component="p">
              {`You will be presented with ${this.props.options.numQuestions} true or false questions.  Try to get the best score, remember time counts!`}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Link
            to="/quiz"
          >
            <Button size="small" color="primary">
              Start
            </Button>
          </Link>
        </CardActions>
      </Card>
    );
  }
}

Trivia.propTypes = {
  options: PropTypes.object,
};

export default withRouter(connect(
  (state) => {return state;},
  null
)(Trivia));