import React from "react";
import PropTypes from "prop-types";
import {
  Switch,
  Route,
  withRouter,
} from "react-router-dom";
import {connect} from "react-redux";
import Grid from '@material-ui/core/Grid';

import Home from "scenes/Home";
import Done from "scenes/Done";
import Questions from "scenes/Questions";
import NotFound from "scenes/NotFound";
import Header from "commonComponents/Header";

import {loadQuestions} from "data/actions";

import "./styles.scss";

class Frame extends React.Component {
  componentWillMount() {
    this.props.dispatch(loadQuestions());
  }

  render() {
    return (
      <div className="page-container">
        <Grid container={true} direction="column" className="app-frame">
          <Grid xs={12}>
            <Header/>
          </Grid>
          <Grid container
                alignItems="center"
                justify="center"
          >
            <Grid item={true} 
                  md={6}
                  xs={12}
            >
              <Switch>
                <Route
                  exact={true}
                  path="/"
                  component={Home}
                />
                <Route
                  exact={true}
                  path="/quiz"
                  component={Questions}
                />
                <Route
                  exact={true}
                  path="/done"
                  component={Done}
                />
                <Route component={NotFound} />
              </Switch>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

Frame.propTypes = {
  dispatch: PropTypes.func,
};

export default withRouter(connect(
  (state) => {return state;},
  null
)(Frame));
