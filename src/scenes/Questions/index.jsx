import React from "react";
import {withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {setAnswer} from "data/actions";

import "./styles.scss";

const secondsToString = secs => {
  var hours = Math.floor(secs / 3600);
  var minutes = Math.floor((secs - (hours * 3600)) / 60);
  var seconds = secs - (hours * 3600) - (minutes * 60);

  if (hours < 10) {hours = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  return minutes+'m '+seconds+'s';
};

class Questions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      time: 0,
    };
  }

  componentDidMount = () => {
    this.startTimer();
  }

  startTimer = () => {
    setInterval(() => {
      this.setState({
        time: this.state.time + 1,
      });
    }, 1000);
  }

  next = () => {
    const newIndex = this.state.index + 1;
    if (newIndex >= this.props.questions.length){
      this.props.history.push('/done');
    }
    else {
      this.setState({
        index: newIndex,
        time: 0,
      });
    }
  }

  setTrue = () => {
    this.props.dispatch(setAnswer(this.state.index, true, this.state.time));
    this.next();
  }
  setFalse = () => {
    this.props.dispatch(setAnswer(this.state.index, false, this.state.time));
    this.next();
  }
  render() {
    if (this.props.questions.length > 0){
      return (
        <Card className="question-card">
          <CardHeader title={this.props.questions[this.state.index].category}/>
          <CardActionArea className='content'>
            <CardContent>
              <Typography component="p" dangerouslySetInnerHTML={{__html: this.props.questions[this.state.index].question}}>
              </Typography>
              <Typography component="p" className="timer">
                Question Timer: {secondsToString(this.state.time)}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button className="bool-btn" size="small" color="primary" onClick={this.setTrue}>
              True
            </Button>
            <Button className="bool-btn" size="small" color="primary" onClick={this.setFalse}>
              False
            </Button>
          </CardActions>
        </Card>
      );
    }
    else {
      return (
        <Card>
          <CardHeader title="Loading...."/>
        </Card>
      );
    }
  }
}

Questions.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object,
  questions: PropTypes.array,
};

export default withRouter(connect(
  (state) => {return state;},
  null
)(Questions));