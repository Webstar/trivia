import React from "react";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Trivia from "scenes/Trivia";

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

import {setOptions, loadQuestions} from "data/actions";

import "./styles.scss";

class Home extends React.Component {
  updateDifficulty = event => {
    const opts = Object.assign({}, this.props.options, {
      difficulty: event.target.value,
    });
    this.props.dispatch(setOptions(opts));
    this.props.dispatch(loadQuestions());
  }
  updateNumQuestions = event => {
    const opts = Object.assign({}, this.props.options, {
      numQuestions: event.target.value,
    });
    this.props.dispatch(setOptions(opts));
    this.props.dispatch(loadQuestions());
  }
  render() {
    return (
      <Grid container
            className="home"
            direction="column"
            spacing={16}
      >
        <Grid item>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography>Options</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container
                className="home"
                direction="column"
                spacing={16}
              >
                <Grid item>
                  <InputLabel htmlFor="filled-numQuestions-simple">Number of Questions</InputLabel>
                  <Select
                    value={this.props.options.numQuestions}
                    onChange={this.updateNumQuestions}
                    fullWidth={true}
                    inputProps={{
                      name: 'numQuestions',
                      id: 'numQuestions-simple',
                    }}
                  >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                  </Select>
                </Grid>
                <Grid item>
                  <InputLabel htmlFor="filled-difficulty-simple">Question Difficulty</InputLabel>
                  <Select
                    value={this.props.options.difficulty}
                    onChange={this.updateDifficulty}
                    fullWidth={true}
                    inputProps={{
                      name: 'difficulty',
                      id: 'difficulty-simple',
                    }}
                  >
                    <MenuItem value={'easy'}>Easy</MenuItem>
                    <MenuItem value={'medium'}>Medium</MenuItem>
                    <MenuItem value={'hard'}>Hard</MenuItem>
                  </Select>
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item>
          <Trivia/>
        </Grid>
      </Grid>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func,
  options: PropTypes.object,
};

export default withRouter(connect(
  (state) => {return state;},
  null
)(Home));