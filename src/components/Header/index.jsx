import React from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import "./styles.scss";

export default class Header extends React.Component {
  render() {
    return (
      <div className="app-header">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="title" 
                        color="inherit"
                        className={{flexGrow: 1}}
            >
              G2i Trivia
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
