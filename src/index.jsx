import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";

import HotApp from "./App";
import configureStore from "data/createStore";

ReactDOM.render(
  (
    <Provider store={configureStore()}>
      <HotApp />
    </Provider>
  ),
  document.getElementById("react-root")
);
